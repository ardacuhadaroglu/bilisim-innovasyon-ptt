//
//  ViewController.swift
//  Bilisim-Innovasyon-PTT
//
//  Created by Arda Çuhadaroğlu on 07/03/16.
//  Copyright © 2016 Arda Çuhadaroğlu. All rights reserved.
//

import UIKit
import VoiceLayer

class ViewController: UIViewController, VLClientDelegate, VLVoiceMessagePlayerDelegate, VLVoiceMessageRecorderDelegate {

    @IBAction func recordAction(sender: AnyObject) {
        print("Butona basıldı")
        
        var newMessage : VLMessage = VLClient.sharedClient().recorder.recordVoiceMessageInChannel(VLClient.sharedClient().channelWithID("Everyone"), info: ["text":"Iphone_Record"])
    }
    
    @IBAction func stopRecordAction(sender: AnyObject) {
        VLClient.sharedClient().recorder.stopRecording()
    }
    
    @IBOutlet var messageText: UITextField!
    
    @IBAction func sendTextAction(sender: AnyObject) {
        print("Send text button : \(messageText.text!)")
        
        var newMessage : VLMessage = VLClient.sharedClient().postTextMessageInChannel(VLClient.sharedClient().channelWithID("Everyone"), info: ["text": messageText.text as! AnyObject ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        VLClient.sharedClient().delegate = self;
        VLClient.sharedClient().player.delegate = self;
        VLClient.sharedClient().recorder.delegate = self;
        
        VLClient.sharedClient().connectWithCompletion({ () -> Void in
            print("Voicelayer connection success")
            }) { (error) -> Void in
                print("Voicelayer connection error")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func voiceLayerDidConnect(client: VLClient!) {
        VLClient.sharedClient().subscribeToChannelsWithIDs(["Everyone"])
    }

    func voiceLayer(client: VLClient!, succeedToSubscribeToChannel channel: VLChannel!) {
        print("succeedToSubscribeToChannel:" + channel.channelID)
        var currentChannel : VLChannel = channel
    }
    
    
    
    func voiceLayerMessageRecorder(recorder: VLVoiceMessageRecorder!, startedRecordingVoiceMessage message: VLMessage!) {
        print("Started recording")
    }
    
    func voiceLayerMessageRecorder(recorder: VLVoiceMessageRecorder!, finishedRecordingVoiceMessage message: VLMessage!, withDuration duration: NSTimeInterval) {
        print("finished recording")
    }
    
    func voiceLayerMessageRecorder(recorder: VLVoiceMessageRecorder!, failedToRecordVoiceMessageWithID message: String!, withError error: NSError!) {
        print("failed recording")
        print(message)
        print(error)
    }
    
    
    
    func voiceLayer(client: VLClient!, succeedToPostMessage message: VLMessage!) {
        print("succeedToPostMessage: ")
        if (message.type == VLMessageType.Voice) {
            print("Şuanda Voice mesaj yollanıyor")
//            VLClient.sharedClient().player.playMessage(message)
        } else if (message.type == VLMessageType.Text) {
            print("Text message yollandı: \(message.info)")
        }
    }
    
    func voiceLayer(client: VLClient!, failedToPostMessage message: VLMessage!, withError error: NSError!) {
        print("failedToPostMessage")
    }
    
    func voiceLayer(client: VLClient!, messageWasPosted message: VLMessage!) {
        print("messageWasPosted")
        
//        VLClient.sharedClient().player.playMessage(message)
    }
    
    func voiceLayer(client: VLClient!, messageWasUpdated message: VLMessage!) {
        print("messageWasUpdated")
    }
   
    func voiceLayer(client: VLClient!, messageWasRemoved message: VLMessage!) {
        print("messageWasRemoved")
    }
    
    func voiceLayer(client: VLClient!, channel: VLChannel!, succeedToFetchMessages messages: [AnyObject]!) {
        print("succeedToFetchMessages")
    }
    
    func voiceLayer(client: VLClient!, channel: VLChannel!, failedToFetchMessagesWithError error: NSError!) {
        print("failedToFetchMessagesWithError")
    }
    
    
    
    
    func voiceLayerMessagePlayer(player: VLVoiceMessagePlayer!, startedPlayingVoiceMessage message: VLMessage!) {
        print("startedPlayingVoiceMessage")
    }
    
    func voiceLayerMessagePlayer(player: VLVoiceMessagePlayer!, interruptedVoiceMessageDueLowBandWidth message: VLMessage!) {
        print("interruptedVoiceMessageDueLowBandWidth")
    }
    
    func voiceLayerMessagePlayer(player: VLVoiceMessagePlayer!, resumedVoiceMessageAfterInterruption message: VLMessage!) {
        print("resumedVoiceMessageAfterInterruption")
    }
    
    func voiceLayerMessagePlayer(player: VLVoiceMessagePlayer!, finishedPlayingVoiceMessage message: VLMessage!) {
        print("finishedPlayingVoiceMessage")
    }
    
    func voiceLayerMessagePlayer(player: VLVoiceMessagePlayer!, failedToPlayVoiceMessage message: VLMessage!, withError error: NSError!) {
        print("failedToPlayVoiceMessage")
    }
    
}

